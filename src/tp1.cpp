
//! \file tuto9.cpp utilisation d'un shader 'utilisateur' pour afficher un objet Mesh

#include "mat.h"
#include "mesh.h"
#include "wavefront.h"

#include "orbiter.h"
#include "program.h"
#include "uniforms.h"
#include "draw.h"

#include "app.h"        // classe Application a deriver

#include <sstream>
#include <string>
//#include <opencv2/opencv.hpp>

class TP : public App
{
public:
    // constructeur : donner les dimensions de l'image, et eventuellement la version d'openGL.
    TP( ) : App(1024, 640) {}
    
    int init( )
    {        
        // etape 1 : creer le shader program
        m_program= read_program("data/shaders/mine_2.glsl");
        program_print_errors(m_program);
        
        // etat openGL par defaut
        glClearColor(0.2f, 0.2f, 0.2f, 1.f);        // couleur par defaut de la fenetre
        
        glClearDepth(1.f);                          // profondeur par defaut
        glDepthFunc(GL_LESS);                       // ztest, conserver l'intersection la plus proche de la camera
        glEnable(GL_DEPTH_TEST);                    // activer le ztest

        robot = read_mesh("data/run/Robot_000001.obj");

        for (int i = 0; i < robot.materials().count(); i++) {
            Color c = robot.materials().materials[i].diffuse;
            colors.emplace_back(vec3(c.r, c.g, c.b));
        }

        for (int i = 0; i < robot.triangle_count(); i++) {
            points.push_back(robot.triangle(i).a);
            points.push_back(robot.triangle(i).b);
            points.push_back(robot.triangle(i).c);

            color_indice.push_back(robot.material_indices()[i]);
            color_indice.push_back(robot.material_indices()[i]);
            color_indice.push_back(robot.material_indices()[i]);

            bool bb = robot.has_normal();

            std::cout << std::endl;
        }

        for (unsigned int i = 0; i < robot.triangle_count(); i++)
            indices.push_back({ 3 * i, 3 * i + 1, 3 * i + 2 });

        for (int i = 1; i < 24; i++) {
            std::stringstream path_start; path_start << "data/run/Robot_0000";
            std::stringstream path_end; path_end << ".obj";
            std::stringstream path, num;

            if (i < 10)
                num << '0' << i;
            else
                num << i;

            path << path_start.str() << num.str() << path_end.str();

            anims.emplace_back(read_mesh(path.str().c_str()));

            std::vector<vec3> anim_point;
            std::vector<vec3> anim_normal;
            for (int j = 0; j < anims[i - 1].triangle_count(); j++) {
                anim_point.push_back(anims[i - 1].triangle(j).a);
                anim_point.push_back(anims[i - 1].triangle(j).b);
                anim_point.push_back(anims[i - 1].triangle(j).c);

                anim_normal.push_back(anims[i - 1].triangle(j).na);
                anim_normal.push_back(anims[i - 1].triangle(j).nb);
                anim_normal.push_back(anims[i - 1].triangle(j).nc);
            }
            anims_points.emplace_back(anim_point);
            anims_normals.emplace_back(anim_normal);
        }

        glGenBuffers(5, vbo);
        
        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);

        glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * anims_points[0].size(), anims_points[0].data(), GL_STATIC_DRAW);
        attribute = glGetAttribLocation(m_program, "p");
        if (attribute < 0)
            return -1;

        glVertexAttribPointer(
            attribute,
            3,
            GL_FLOAT,
            GL_FALSE,
            0,
            (void*)0);
        glEnableVertexAttribArray(attribute);
        
        glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * anims_points[1].size(), anims_points[1].data(), GL_STATIC_DRAW);
        attribute0 = glGetAttribLocation(m_program, "p0");
        if (attribute0 < 0)
            return -1;

        glVertexAttribPointer(
            attribute0,
            3,
            GL_FLOAT,
            GL_FALSE,
            0,
            (void*)0);
        glEnableVertexAttribArray(attribute0);

        glBindBuffer(GL_ARRAY_BUFFER, vbo[3]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * anims_normals[0].size(), anims_normals[0].data(), GL_STATIC_DRAW);
        attribute2 = glGetAttribLocation(m_program, "_normal0");
        if (attribute2 < 0)
            return -1;

        glVertexAttribPointer(
            attribute2,
            3,
            GL_FLOAT,
            GL_FALSE,
            0,
            (void*)0);
        glEnableVertexAttribArray(attribute2);

        glBindBuffer(GL_ARRAY_BUFFER, vbo[4]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vec3)* anims_normals[1].size(), anims_normals[1].data(), GL_STATIC_DRAW);
        attribute3 = glGetAttribLocation(m_program, "_normal1");
        if (attribute3 < 0)
            return -1;

        glVertexAttribPointer(
            attribute3,
            3,
            GL_FLOAT,
            GL_FALSE,
            0,
            (void*)0);
        glEnableVertexAttribArray(attribute3);

        glBindBuffer(GL_ARRAY_BUFFER, vbo[2]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(int)* color_indice.size(), color_indice.data(), GL_STATIC_DRAW);
        attribute1 = glGetAttribLocation(m_program, "color_id");
        if (attribute1 < 0)
            return -1;

        glVertexAttribIPointer(
            attribute1,
            1,
            GL_INT,
            0,
            (void*)0);
        glEnableVertexAttribArray(attribute1);

        //Ceeation texture
        glGenTextures(1, &color_texture);
        glBindTexture(GL_TEXTURE_2D, color_texture);

        glTexImage2D(
            GL_TEXTURE_2D,
            0,
            GL_RGBA,
            window_width(),
            window_height(),
            0,
            GL_RGBA,
            GL_UNSIGNED_BYTE,
            nullptr);

        glGenTextures(1, &depth_texture);
        glBindTexture(GL_TEXTURE_2D, depth_texture);

        glTexImage2D(GL_TEXTURE_2D, 0,
            GL_DEPTH_COMPONENT,
            window_width(),
            window_height(), 0,
            GL_DEPTH_COMPONENT, GL_UNSIGNED_INT, nullptr);

        glGenerateMipmap(GL_TEXTURE_2D);
        glGenSamplers(1, &color_sampler);

        glSamplerParameteri(color_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glSamplerParameteri(color_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(color_sampler, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        glSamplerParameteri(color_sampler, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
        
        // Creation framebuffer
        framebuffer;
        glGenFramebuffers(1, &framebuffer);
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, framebuffer);
        glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, color_texture, 0);
        glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depth_texture, 0);

        GLenum buffers[] = { GL_COLOR_ATTACHMENT0 };
        glDrawBuffers(1, buffers);
       
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

        glClearDepthf(1.f);                          // profondeur par defaut
        glDepthFunc(GL_LESS);                       // ztest, conserver l'intersection la plus proche de la camera
        glEnable(GL_DEPTH_TEST);

        return 0;   // ras, pas d'erreur
    }
    
    // destruction des objets de l'application
    int quit( )
    {
        // etape 3 : detruire le shader program
        release_program(m_program);
        robot.release();

        for (int i = 0; i < anims.size(); i++)
            anims[i].release();
        return 0;
    }
    
    // dessiner une nouvelle image
    int render( )
    {
        
        // deplace la camera
        int mx, my;
        unsigned int mb= SDL_GetRelativeMouseState(&mx, &my);
        if(mb & SDL_BUTTON(1))              // le bouton gauche est enfonce
            m_camera.rotation(mx, my);
        else if(mb & SDL_BUTTON(3))         // le bouton droit est enfonce
            m_camera.move(mx);
        else if(mb & SDL_BUTTON(2))         // le bouton du milieu est enfonce
            m_camera.translation((float) mx / (float) window_width(), (float) my / (float) window_height());
        
        elapsed += delta_time() * 24;

        // etape 2 : dessiner m_objet avec le shader program
        // configurer le pipeline 
        glUseProgram(m_program);
        
        // configurer le shader program
        // . recuperer les transformations
        //Transform model= RotationX(global_time() / 20);
        //Transform model= Identity() * Translation(0, 0, 1.5f);
        Transform model= Identity() * Scale(0.5f, 0.5f, 0.5f) * RotationY(0.01f) * Translation(0,-2,0);
        Transform rodel = Identity() * RotationY(0.01f);

        Transform view = m_camera.view();
        Transform projection= m_camera.projection(window_width(), window_height(), 45);
        
        // . composer les transformations : model, view et projection
        Transform mvp= projection * view * model;
        Transform mv = view * model;
        Transform m = rodel;
        
        // . parametrer le shader program :
        //   . transformation : la matrice declaree dans le vertex shader s'appelle mvpMatrix
        //program_uniform(m_program, "mvpMatrix", mvp);
        
        int i = ((int)(elapsed / 1000.0f)) % anims_points.size();
        int ip1 = (i + 1) % anims_points.size();

        float m_elapsed = (elapsed / 1000.0f) - (int)(elapsed / 1000.0f);
        
        GLint mvpMatrix_location = glGetUniformLocation(m_program, "mvpMatrix");
        glUniformMatrix4fv(mvpMatrix_location, 1, GL_TRUE, &mvp.m[0][0]); // GL_TRUE car row major

        GLint mvMatrix_location = glGetUniformLocation(m_program, "mvMatrix");
        glUniformMatrix4fv(mvMatrix_location, 1, GL_TRUE, &mv.m[0][0]); // GL_TRUE car row major

        GLint mMatrix_location = glGetUniformLocation(m_program, "mMatrix");
        glUniformMatrix4fv(mMatrix_location, 1, GL_TRUE, &m.m[0][0]); // GL_TRUE car row major
        
        GLint elapsed_location = glGetUniformLocation(m_program, "elapsed");
        glUniform1f(elapsed_location, elapsed / 1000.0f);

        GLint colors_location = glGetUniformLocation(m_program, "colors");
        glUniform3fv(colors_location, colors.size(), (float*)colors.data());

        GLint m_elapsed_location = glGetUniformLocation(m_program, "w");
        glUniform1f(m_elapsed_location, m_elapsed);

        //   . ou, directement en utilisant openGL :
        //   int location= glGetUniformLocation(program, "mvpMatrix");
        //   glUniformMatrix4fv(location, 1, GL_TRUE, mvp.buffer());
        
        // . parametres "supplementaires" :
        //   . couleur des pixels, cf la declaration 'uniform vec4 color;' dans le fragment shader
        ////program_uniform(m_program, "color", vec4(1, 1, 0, 1));
        //   . ou, directement en utilisant openGL :
        //   int location= glGetUniformLocation(program, "color");
        //   glUniform4f(location, 1, 1, 0, 1);
        
        // go !
        // indiquer quels attributs de sommets du mesh sont necessaires a l'execution du shader.
        // tuto9_color.glsl n'utilise que position. les autres de servent a rien.
        //m_objet.draw(m_program, /* use position */ true, /* use texcoord */ false, /* use normal */ false, /* use color */ false, /* use material index*/ false);
        
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, framebuffer); // Destination d'ecriture : framebuffer
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, color_texture);
        glBindSampler(0, color_sampler);

        glClearColor(0, 0, 0, 1);
        glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * anims_points[i].size(), anims_points[i].data(), GL_STATIC_DRAW);
        
        glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * anims_points[ip1].size(), anims_points[ip1].data(), GL_STATIC_DRAW);

        glBindBuffer(GL_ARRAY_BUFFER, vbo[3]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * anims_normals[i].size(), anims_normals[i].data(), GL_STATIC_DRAW);

        glBindBuffer(GL_ARRAY_BUFFER, vbo[4]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * anims_normals[ip1].size(), anims_normals[ip1].data(), GL_STATIC_DRAW);
        
        glDrawArrays(GL_TRIANGLES, 0, points.size());
        
        if (key_state(' '))
        {
            /* montrer le resultat de la passe 1
                copie le framebuffer sur la fenetre
             */

            glBindFramebuffer(GL_READ_FRAMEBUFFER, framebuffer); // Source de la lecture: framebuffer
            glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0); // Destination d'ecriture : ecran
            glClearColor(0.2f, 0.2f, 0.2f, 1);
            glClear(GL_COLOR_BUFFER_BIT);

            const int sub_width = 300;
            const int sub_height = 400;

            GLubyte b[sub_width * sub_height * 4];
            GLubyte _b[sub_width * sub_height * 4];

            glReadPixels(500, 150, sub_width, sub_height, GL_RGBA, GL_UNSIGNED_BYTE, _b);

            for (int i = 0; i < sub_width * sub_height * 4; i += 4) {
                _b[i] = 255 - _b[i];
                _b[i + 1] = 255 - _b[i + 1];
                _b[i + 2] = 255 - _b[i + 2];
            }

            glTexSubImage2D(GL_TEXTURE_2D,
                0,
                500, 150, sub_width, sub_height,
                GL_RGBA,
                GL_UNSIGNED_BYTE,
                _b);

            glBlitFramebuffer(
                0, 0, window_width(), window_height(),        // rectangle origine dans READ_FRAMEBUFFER
                0, 0, window_width(), window_height(),        // rectangle destination dans DRAW_FRAMEBUFFER
                GL_COLOR_BUFFER_BIT, GL_LINEAR);                        // ne copier que la couleur (+ interpoler)
        }
        else {
            glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0); // Destination de draw : Par default (ecran)

            glClearColor(0.2f, 0.2f, 0.2f, 1);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glDrawArrays(GL_TRIANGLES, 0, points.size());
        }

        return 1;
    }

protected:
    Transform m_model;
    Orbiter m_camera;
    GLuint m_texture;
    GLuint m_program;
    float elapsed = 0;

    Mesh robot;
    std::vector<Mesh> anims;
    std::vector<std::vector<vec3>> anims_points;
    std::vector<std::vector<vec3>> anims_normals;
    std::vector<vec3> points;
    std::vector<vec3> normals;
    std::vector<int> color_indice;

    std::vector<vec3> colors;

    GLuint framebuffer;
    GLuint color_texture;
    GLuint depth_texture;
    GLuint color_sampler; // k�c�c� ?

    // Pipeline

    //GLuint vbo; // Vertex Buffer Objet : Contient les information pour chaque vertex
    GLuint vbo[5]; // Vertex Buffer Objet : Contient les information pour chaque vertex
    GLuint vao; // Vertex Attribute Objet : Comme l'input layer en Direct X, continent la forme des attribut, genre ou est quoi

    GLint attribute;
    GLint attribute0;
    GLint attribute1;
    GLint attribute2;
    GLint attribute3;

    struct ivec3 {
        unsigned int x, y, z;
    };

    //std::vector<unsigned int> indices;
    std::vector<ivec3> indices;
};


int main( int argc, char **argv )
{
    TP tp;
    tp.run();
    
    return 0;
}
