
//! \file shader_kit.cpp shader_kit light, bac a sable fragment shader, cf shader_toy 

#include <cstdio>
#include <cstring>
#include <iostream>
#include <sstream>
#include <fstream>

#include "glcore.h"
#include "window.h"
#include "app.h"

#include "program.h"
#include "uniforms.h"

#include "texture.h"
#include "mesh.h"
#include "wavefront.h"

#include "vec.h"
#include "mat.h"
#include "orbiter.h"
#include "draw.h"

#include "text.h"
#include "widgets.h"
#include <chrono>
#include <omp.h>
#include <random>

struct World
{
    World(const Vector& _n) : n(_n)
    {
        float sign = std::copysign(1.0f, n.z);
        float a = -1.0f / (sign + n.z);
        float d = n.x * n.y * a;
        t = Vector(1.0f + sign * n.x * n.x * a, sign * d, -sign * n.x);
        b = Vector(d, sign + n.y * n.y * a, -n.y);
    }

    // transforme le vecteur du repere local vers le repere du monde
    Vector operator( ) (const Vector& local)  const { return local.x * t + local.y * b + local.z * n; }

    // transforme le vecteur du repere du monde vers le repere local
    Vector inverse(const Vector& global) const { return Vector(dot(global, t), dot(global, b), dot(global, n)); }

    Vector t;
    Vector b;
    Vector n;
};

struct Ray
{
    Point o;            // origine
    Vector d;           // direction

    Ray(const Point& _o, const Point& _e) : o(_o), d(normalize(Vector(_o, _e))) {}
};

struct Hit
{
    float t;            // p(t)= o + td, position du point d'intersection sur le rayon
    float u, v;         // p(u, v), position du point d'intersection sur le triangle
    //int triangle_id;    // indice du triangle dans le mesh
    bool intersect;

    // pas d'intersection
    Hit() : t(FLT_MAX), u(), v(), intersect(false) {}
    // intersection 
    Hit(const float _t, const float _u, const float _v, const bool _intersect) : t(_t), u(_u), v(_v), intersect(_intersect) {}

    // renvoie vrai si l'intersection est valide
    operator bool() { return intersect; }
};

/*struct Sphere
{
    Point c;
    double r;
    Color diffuse;

    Hit intersect(const Ray& ray, const float tmax) const {
        Hit h;

        //float a = dot(ray.d, ray.d);
        float a = length2(ray.d);
        float b = dot(2 * ray.d, ray.o - c);
        float k = dot(ray.o - c, ray.o - c) - r * r;

        if (b * b - 4.0 * a * k > 0) {
            float t1 = (-b - sqrt(b * b - 4.0 * a * k)) / (2.0 * a);
            float t2 = (-b + sqrt(b * b - 4.0 * a * k)) / (2.0 * a);
            float t = std::min(t1, t2);

            if (t > 0 && t < tmax) {
                h.intersect = true;
                h.t = t;
            }
        }
        else {
            h.intersect = false;
        }

        return h;
    }
};*/

struct Triangle
{
    Point p;
    Vector e1, e2;
    int id;
    Color diffuse;

    Color emission;
    float area;
    Vector normal;

    Triangle() {}
    Triangle(const Point& _a, const Point& _b, const Point& _c, const int _id) : p(_a), e1(Vector(_a, _b)), e2(Vector(_a, _c)), id(_id) {
        float fa = length(Vector(_a) - Vector(_b));
        float fb = length(Vector(_b) - Vector(_c));
        float fc = length(Vector(_c) - Vector(_a));
        float p = (fa + fb + fc) / 2.f;
        area = sqrt(p * (p - fa) * (p - fb) * (p - fc));
        
        normal = cross(normalize(e1), normalize(e2));
    }

    Hit intersect(const Ray& ray, const float htmax) const
    {
        
        Hit h;
        Vector pvec = cross(ray.d, e2);
        float det = dot(e1, pvec);

        float inv_det = 1 / det;
        Vector tvec(p, ray.o);

        float u = dot(tvec, pvec) * inv_det;
        if (u < 0 || u > 1) {
            h.intersect = false;
            return h;
        }

        Vector qvec = cross(tvec, e1);
        float v = dot(ray.d, qvec) * inv_det;
        if (v < 0 || u + v > 1) {
            h.intersect = false;
            return h;
        };

        float t = dot(e2, qvec) * inv_det;
        if (t > htmax || t < 0) {
            h.intersect = false;
            return h;
        };

        h.intersect = true;
        h.t = t;
        h.u = u;
        h.v = v;

        return h;           // p(u, v)= (1 - u - v) * a + u * b + v * c
    }

    Point sample(const float u1, const float u2) const
    {
        // cf GI compemdium eq 18
        float r1 = std::sqrt(u1);
        float alpha = 1 - r1;
        float beta = (1 - u2) * r1;
        float gamma = u2 * r1;
        return alpha * p + beta * (p + e1) + gamma * (p + e2);
    }

    float pdf(const Point& p) const
    {
        return 1.0 / area;
    }
};

Orbiter camera;

// Donne le reflet du vecteur d par rapport a la surface de normal n
Vector reflect(Vector d, Vector n) {
    n = normalize(n);
    
    float _dot = dot(d, n);
    n = n * 2.0f * _dot;
    return d - n;
}

// Donne le symetrique du point p par rapport au point axe
Vector symetric(Vector p, Vector axe) {
    return 2.0 * axe - p;
}

// Genere un point aleatoire dans une hemisphere unitaire
// Centre : Le point centrale de la sphere de l'hemisphere
// Normal : Le vecteur qui va du point centrale vers le sommet du dome
// u1, u2 : Deux nombres al�atoires entre 0 et 1
Vector hemisphere_sample(const Vector centre, const Vector normal, const float rayon, const float u1, const float u2) {
    // cf GI Compemdium Eq. 33 + Petite tricherie
    float r1 = u1;
    float r2 = u2;

    float p1 = 2.0 * M_PI * r1;
    float p2 = sqrt(r2 * (1.0 - r2));

    float x = centre.x + 2.0 * cos(p1) * p2;
    float y = centre.y + 2.0 * sin(p1) * p2;
    float z = centre.z + (1.0 - (2.0 * r2));
    Vector res(x, y, z);

    // La petite tricherie : on ramene tout les points de l'hemisphere oppos�e dans le bon hemisphere
    // Dot product positif == Point dans l'autre hemisphere, on prend le symetrique du point trouve par rapport au centre
    if (dot(res, normal) > 0.0f)
        res = symetric(res, centre);

    return res;
}

Vector hemisphere_sample_angle(float u1, float u2) {
    float r1 = u1;
    float r2 = u2;

    float p1 = 2.0f * M_PI * r1;
    float p2 = sqrt(r2 * (1.0f - r2));

    float x = cos(2.0f * M_PI * r1) * sqrt(1 - r2 * r2);
    float y = sin(2.0f * M_PI * r1) * sqrt(1 - r2 * r2);
    float z = r2;

    return Vector(x,y,z);
}

// 0 < teta_max < PI/2
Vector pre_hemisphere_sample_angle(float teta_max, std::default_random_engine& rng) {
    float u1, u2;
    std::uniform_real_distribution<float> _u1(0.f, 1.f);
    std::uniform_real_distribution<float> _u2(0.f, 1.f);
    //std::uniform_real_distribution<float> _u2(0.f, cos(teta_max));
    u1 = _u1(rng);
    //u2 = _u2(rng);
    do { u2 = _u2(rng); } while (acos(u2) > teta_max);
    return hemisphere_sample_angle(u1, u2);
    
    //return hemisphere_sample_angle(u1, u2 * cos(teta_max));
};

// PDF d'une hemisphere unitaire
float hemisphere_pdf() {
    return 1.0 / (2.0 * M_PI);
}

// PDF d'une hemisphere
float hemisphere_pdf_angle(float teta_max) {
    return 1.0 / (2.0 * M_PI * cos(teta_max));
}

// Calcul la couleur d'un sample pour une direction donn�e
Color getSampleColor(
    const std::vector<Triangle>& triangles, // Liste des triangles
    const std::vector<Triangle>& sources, // Les soucres de lumi�re
    const Point p, // Point de collision (origin du aussi reflet)
    const vec3 normal, // Normal au point de collision
    const int id, // Id de l'objet qu'on a touch�
    const Ray ray, // Rayon utilis� pour arriver ici (peu servir pour les reflets)
    const int sample_count, // Nombre de sample a prendre
    const int bounce, // Nombre de rebond pour composer le sample
    std::default_random_engine& rng // Generateur de nombre aleatoire
    ) {

    Color final_color = Color(0.0f, 0.0f, 0.0f);

    for (int i = 0; i < sample_count; i++) {
        // un generateur par thread... pas de synchronisation
        // nombres aleatoires entre 0 et 1

        Color c = Color(0, 0, 0);

        for(int k = 0; k < sources.size(); k++) {
            std::uniform_real_distribution<float> u1(0.f, 1.f);
            std::uniform_real_distribution<float> u2(0.f, 1.f);

            unsigned int rand_source_id = rand() % sources.size();
            Triangle s = sources[k];
            Point _s = s.sample(u1(rng), u2(rng));

            Hit hit2;
            int id2 = -1;
            hit2.t = length(p - _s) / length(ray.d);
            bool collision = false;
            Ray ray2 = Ray(p, _s); // La source de lumi�re

            for (int i = 0; i < triangles.size(); i++) {
                if (i != s.id && i != id) {
                    Hit h = triangles[i].intersect(ray2, hit2.t);
                    if (h && triangles[i].id != s.id) {
                        collision = true;
                        id2 = i;
                    }
                }
            }

            if (!collision) {
                float dist = length(p - _s); // Distance objet <-> source de lumi�re
                float ang_a_source = std::max(0.0f, dot(s.normal, normalize(Vector(p - _s)))); // Exposition � la source
                float ang_a_objet = std::max(0.0f, dot(normalize(p - _s), -normal));

                float d = (ang_a_source * ang_a_objet) / (dist * dist);

                c = c + (triangles[id].emission + triangles[id].diffuse * s.emission * d) / s.pdf(_s) / sources.size();
            }
        }

        // Calcul des reflets (ind�pendant de si on a touch� une source de lumi�re pr�cedement on pas)
        if (bounce > 0) {
            // Angle pour la "diffusion du mat�riaux"
            // Plus l'angle est petit, plus le mat�riaux est reflechissant
            float teta_max = M_PI / 4.0f;

            Vector reflect_dir_diff = reflect(ray.d, normal);

            World w(reflect_dir_diff);

            Vector reflect_dir = pre_hemisphere_sample_angle(teta_max, rng);
            reflect_dir = w(reflect_dir);

            Ray ray_reflect(p, p + Point(reflect_dir));

            float z_buffer = 10000.0f;

            Hit hit_reflect;
            int id_reflect;
            for (int i = 0; i < triangles.size(); i++) {
                if (i != id) {
                    Hit h = triangles[i].intersect(ray_reflect, hit_reflect.t);
                    if (z_buffer > h.t) {
                        hit_reflect = h;
                        id_reflect = i;
                        z_buffer = h.t;
                    }
                }
            }

            // Si le reflet touche un objet, on recupere la couleur de cette objet et on l'ajoute a notre sample
            if (hit_reflect) {
                Point p_reflect = ray_reflect.o + hit_reflect.t * ray_reflect.d; // Objet a refleter
                Vector normal_reflect; // Normale de l'objet a refleter

                float reflect_dist = length(p - p_reflect);

                normal_reflect = normalize(
                    cross(
                        triangles[id_reflect].e1,
                        triangles[id_reflect].e2));

                p_reflect = p_reflect + 0.001 * normal_reflect; // Exposition � l'objet reflet� (comme une source de lumi�re)

                float angle_reflect = dot(normal_reflect, -reflect_dir);

                Color indirect = getSampleColor(triangles, sources, p_reflect, normal_reflect, id_reflect, ray_reflect, std::max(sample_count / 2, 1), bounce - 1, rng); // 
                indirect = indirect /*std::max(angle_reflect, 0.0f)*/ /*triangles[id].diffuse * std::max(angle_reflect, 0.0f) */* hemisphere_pdf_angle(teta_max)/* / (float)std::max(sample_count / 4, 1)*/;
                c = c + indirect;
            }
        }

        final_color = final_color + c;
    }
    final_color = final_color / (float)sample_count;

    return final_color;
}

int main(int argc, char** argv) {
    srand(time(0));

    std::random_device seed;
    std::default_random_engine rng(seed());
    
    //const int w = 1024;
    //const int h = 768;
    const int w = 800;
    const int h = 800;

    Mesh mesh = read_mesh("data/cornell.obj");

    Image image(w, h);

    //camera.read_orbiter("data/cornell_orbiter.txt");
    camera.read_orbiter("orbiter.txt");

    camera.projection(image.width(), image.height(), 45);

    //Transform model = Identity() * RotationX(-45);
    Transform model = Identity();
    Transform view = camera.view();
    Transform projection = camera.projection();
    Transform viewport = camera.viewport();
    Transform inv = Inverse(viewport * projection * view * model);

    //std::vector<Sphere> spheres;
    std::vector<Triangle> triangles;
    std::vector<Triangle> sources;

    for (int i = 0; i < mesh.triangle_count(); i++) {
        Triangle t(mesh.triangle(i).a, mesh.triangle(i).b, mesh.triangle(i).c, i);

        int color_id = mesh.material_indices()[i];
        t.diffuse = mesh.materials().materials[color_id].diffuse;
        t.emission = mesh.materials().materials[color_id].emission;

        if (mesh.materials().materials[color_id].emission.power() > 0)
            sources.push_back(t);
        triangles.push_back(t);
    }

    auto start = std::chrono::high_resolution_clock::now();

    #pragma omp parallel for
    for(int k = 0; k < w * h; k++) {
        int x = k % w;
        int y = k / w;
        float z_buffer = 10000.0f;
        image(x, y) = Color(0, 0, 0);

        Point origine = inv(Point(x + .5f, y + .5f, 0));
        Point extremite = inv(Point(x + .5f, y + .5f, 1));
        Ray ray(origine, extremite);

        // 1. On cherche avec quel objet on se collisionne
        Hit hit;
        int id;
        for (int i = 0; i < triangles.size(); i++) {
            Hit h = triangles[i].intersect(ray, hit.t);
            if (z_buffer > h.t) {
                hit = h;
                z_buffer = h.t;
                id = i;
            }
        }

        Color final_color = Color(0, 0, 0);

        // Si on touche un objet
        if (hit) {
            Point p = ray.o + hit.t * ray.d;
            Vector normal;

            normal = normalize(
                cross(
                    normalize(triangles[id].e1),
                    normalize(triangles[id].e2)));

            p = p + 0.001 * normal;

            // V2 : On teste avec des samples et on les mixes
            int sample_count = 32;
            int bounce = 1;
            final_color = getSampleColor(triangles, sources, p, normal, id, ray, sample_count, bounce, rng);
            
            // Tone Mapping
            float gamma = 1.4f;
            final_color.r = pow(final_color.r, 1.0f / gamma);
            final_color.g = pow(final_color.g, 1.0f / gamma);
            final_color.b = pow(final_color.b, 1.0f / gamma);

            final_color.a = 1.0f;

            image(x, y) = final_color;
        } else {
            // Si on ne touche rien on affiche rien (ou une couleur de debug)
            image(x, y) = Color(0,0,0);
        }
    }

    auto stop = std::chrono::high_resolution_clock::now();
    int cpu = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start).count();
    printf("%dms\n", cpu);

    write_image(image, "render.png");

    return 0;
}
