#version 330

#ifdef VERTEX_SHADER

//layout(location = 0) in vec3 position;
in vec3 p;
//layout(location = 1) in vec3 p0;
in vec3 p0;
in vec3 _normal0;
in vec3 _normal1;
in int color_id;
uniform mat4 mvpMatrix;
uniform mat4 mvMatrix;
uniform mat4 mMatrix;
uniform float elapsed;
uniform float w;

flat out int id_color;
out vec3 n;

void main()
{
    float select = 0.01f;

    //float ff = (cos(elapsed) + 1.0f) / 2.0f;
    
    //vec3 pp = select * p + (1.0f - select) * p0;
    vec3 pp = (1.0f - w) * p + w * p0;
    
    //gl_Position = mvpMatrix * vec4(p0, 1);
    id_color = color_id;
    n = (1.0f - w) * _normal1 + w * _normal0;

    gl_Position = mvpMatrix * vec4(pp, 1);
}
#endif


#ifdef FRAGMENT_SHADER

uniform vec3 colors[3];
//uniform vec4 mesh_color = vec4(1, 1, 1, 1);

flat in int id_color;
in vec3 n;
out vec4 fragment_color;


void main()
{
    float s = max(
        0.0f,
        dot(
            //vec3(0, 0, 1),
            vec3(0, 0, 1),
            normalize(n)
        )
    );

    //fragment_color = vec4(colors[id_color],1);
    fragment_color = vec4(colors[id_color] * s , 1);
    //fragment_color = vec4(colors[2],1);
}
#endif
